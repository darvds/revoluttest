package com.darvds.revoluttest.di

import android.content.Context
import android.view.LayoutInflater
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val context: Context) {

    @Provides
    fun provideLayoutInflater() : LayoutInflater{
        return LayoutInflater.from(context)
    }
}