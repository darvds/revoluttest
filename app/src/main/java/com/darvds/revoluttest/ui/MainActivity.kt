package com.darvds.revoluttest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.darvds.revoluttest.di.AppModule
import com.darvds.revoluttest.di.DaggerAppComponent

import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import android.content.Context
import android.view.inputmethod.InputMethodManager
import com.darvds.revoluttest.R
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.itemrow.*


/**
 * Main Activity for the app to show the list of currencies and convert the values
 */
class MainActivity : AppCompatActivity(), MainPresenter.View {

    @Inject
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupRecyclerView()

        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build().inject(this)

        presenter.attachView(this)
        presenter.start()

    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
        presenter.startLoadingExchangeRate()
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }

    /**
     * Setup the look of the recycler view to show a vertical list
     */
    private fun setupRecyclerView(){
        val manager = LinearLayoutManager(this)
        recyclerview.layoutManager = manager
    }

    /**
     * Set the adapter to display items in the recycler view
     */
    override fun setAdapter(adapter: MainAdapter) {
        recyclerview.adapter = adapter
    }

    /**
     * Update a range of rows in the list
     */
    override fun updateRows(startPosition: Int, count: Int) {
        recyclerview.post {
            recyclerview.adapter?.notifyItemRangeChanged(startPosition, count)
        }
    }

    /**
     * Scroll to a specific position
     */
    override fun scrollToPosition(position: Int) {
        recyclerview.scrollToPosition(position)
    }

    /**
     * Grab focus on one of the rows in the list and force the keyboard to show.
     * Add a small delay to allow for the animation to complete and the currency
     * move to the top of the list
     */
    override fun focusItem(position: Int) {
        recyclerview.postDelayed({
            val viewHolder = recyclerview.findViewHolderForAdapterPosition(0) as? CurrencyViewHolder
            val editText = viewHolder?.getEditText()
            viewHolder?.focus()
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }, 200)

    }

    /**
     * Show an error message to the user when the api fails to get new data
     */
    override fun showError(error: Throwable?) {
        Snackbar.make(root_view, com.darvds.revoluttest.R.string.on_error, Snackbar.LENGTH_SHORT)
    }
}
