package com.darvds.revoluttest.ui

import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.darvds.revoluttest.model.CurrencyRow
import com.darvds.revoluttest.utils.CustomTextWatcher
import com.nhaarman.mockito_kotlin.*
import kotlinx.android.synthetic.main.itemrow.view.*
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CurrencyViewHolderTest {

    private lateinit var viewHolder: CurrencyViewHolder

    private val mockView = mock<View>()
    private val mockTextWatcher = mock<CustomTextWatcher>()
    private val mockCallback = mock<MainAdapter.MainAdapterCallback>()

    private val mockTextViewFlag = mock<TextView>()
    private val mockTextViewCurrency = mock<TextView>()
    private val mockTextViewCurrencyName = mock<TextView>()
    private val mockEditText = mock<EditText>()

    @Before
    fun setUp() {
        whenever(mockView.textview_country_flag).thenReturn(mockTextViewFlag)
        whenever(mockView.textview_currency).thenReturn(mockTextViewCurrency)
        whenever(mockView.textview_currency_name).thenReturn(mockTextViewCurrencyName)
        whenever(mockView.edittext_value).thenReturn(mockEditText)


        viewHolder = CurrencyViewHolder(mockView, mockTextWatcher, mockCallback)
    }

    @Test
    fun init_add_textwatcher_and_click_listener(){
        verify(mockEditText).addTextChangedListener(mockTextWatcher)
        verify(mockView).setOnClickListener(any())
    }

    @Test
    fun bind_current() {
        val mockCurrencyRow = mock<CurrencyRow>()
        whenever(mockCurrencyRow.countryCode).thenReturn("GB")
        whenever(mockCurrencyRow.currency).thenReturn("GBP")
        whenever(mockCurrencyRow.currencyName).thenReturn("British Pound")
        whenever(mockCurrencyRow.getFormattedValue(any())).thenReturn("100.00")

        viewHolder.bind(mockCurrencyRow, 0)

        verify(mockTextWatcher).updatePosition(0)
        verify(mockTextViewFlag).text = "\uD83C\uDDEC\uD83C\uDDE7"
        verify(mockTextViewCurrency).text = "GBP"
        verify(mockTextViewCurrencyName).text = "British Pound"
        verify(mockEditText).setText("100.00")
        verify(mockEditText).isEnabled = true

    }

    @Test
    fun bind_non_current(){
        val mockCurrencyRow = mock<CurrencyRow>()
        whenever(mockCurrencyRow.countryCode).thenReturn("GB")
        whenever(mockCurrencyRow.currency).thenReturn("GBP")
        whenever(mockCurrencyRow.currencyName).thenReturn("British Pound")
        whenever(mockCurrencyRow.getFormattedValue(any())).thenReturn("100.00")

        viewHolder.bind(mockCurrencyRow, 2)

        verify(mockTextWatcher).updatePosition(2)
        verify(mockTextViewFlag).text = "\uD83C\uDDEC\uD83C\uDDE7"
        verify(mockTextViewCurrency).text = "GBP"
        verify(mockTextViewCurrencyName).text = "British Pound"
        verify(mockEditText).setText("100.00")
        verify(mockEditText).isEnabled = false
    }

    @Test
    fun getView_returns_view(){
        assertEquals(mockView, viewHolder.getView())
    }

    @Test
    fun getEditText_returns_edittext(){
        assertEquals(mockEditText, viewHolder.getEditText())
    }

    @Test
    fun focus_calls_focus_on_edit_text_when_focusable(){
        whenever(mockEditText.isFocusable).thenReturn(true)
        viewHolder.focus()
        verify(mockEditText).requestFocus()
    }

    @Test
    fun focus_does_not_call_focus_on_edit_text_when_not_focusable(){
        whenever(mockEditText.isFocusable).thenReturn(false)
        viewHolder.focus()
        verify(mockEditText, never()).requestFocus()
    }
}