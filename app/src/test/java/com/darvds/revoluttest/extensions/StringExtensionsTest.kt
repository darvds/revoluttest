package com.darvds.revoluttest.extensions

import org.junit.Assert.*
import org.junit.Test

class StringExtensionsTest{


    @Test
    fun countryEmoji_gb() {
        assertEquals("\uD83C\uDDEC\uD83C\uDDE7", "gb".countryEmoji())
    }


    @Test
    fun countryEmoji_uppercase_us() {
        assertEquals("\uD83C\uDDFA\uD83C\uDDF8", "US".countryEmoji())
    }
}