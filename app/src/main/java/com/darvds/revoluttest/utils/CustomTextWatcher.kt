package com.darvds.revoluttest.utils

import android.text.Editable
import android.text.TextWatcher
import com.darvds.revoluttest.ui.MainAdapter

class CustomTextWatcher(private val callback: MainAdapter.MainAdapterCallback) : TextWatcher {

    private var position: Int = 0

    fun updatePosition(position: Int){
        this.position = position
    }

    override fun afterTextChanged(p0: Editable?) {
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if(position == 0) {
            callback.setBaseValue(charSequence?.toString() ?: "0.0")
        }
    }
}