package com.darvds.revoluttest.utils

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class ObservableHelperImpl : ObservableHelper {

    override fun getRepeatingObservable(initialDelay: Long, timePeriod: Long, timeUnit: TimeUnit): Observable<Long> {
        return Observable.interval(initialDelay, timePeriod, timeUnit)
    }

    override fun getMainThreadScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}