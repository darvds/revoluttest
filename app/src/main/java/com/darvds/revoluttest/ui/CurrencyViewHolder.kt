package com.darvds.revoluttest.ui

import android.view.View
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.darvds.revoluttest.extensions.countryEmoji
import com.darvds.revoluttest.model.CurrencyRow
import com.darvds.revoluttest.utils.CustomTextWatcher
import kotlinx.android.synthetic.main.itemrow.view.*
import java.text.NumberFormat
import javax.security.auth.callback.Callback

/**
 * Viewholder for the currency row to display the country flag, currency, name and the converted value
 */
class CurrencyViewHolder(itemView: View, private val textWatcher: CustomTextWatcher,
                         callback: MainAdapter.MainAdapterCallback) : RecyclerView.ViewHolder(itemView) {

    init {
        itemView.edittext_value.addTextChangedListener(textWatcher)
        itemView.setOnClickListener {
            callback.onItemPressed(this.listPosition)
        }
    }

    /**
     * The current position of this item in the list
     */
    private var listPosition = 0

    /**
     * Bind the view with the data for this currency
     */
    fun bind(currencyRow: CurrencyRow, position: Int){
        this.listPosition = position
        textWatcher.updatePosition(position)

        itemView.textview_country_flag.text = currencyRow.countryCode.countryEmoji()
        itemView.textview_currency.text = currencyRow.currency
        itemView.textview_currency_name.text = currencyRow.currencyName

        val numberFormat = NumberFormat.getNumberInstance()
        numberFormat.maximumFractionDigits = 2
        itemView.edittext_value.setText(currencyRow.getFormattedValue(numberFormat))

        itemView.edittext_value.isEnabled = position == 0
    }

    /**
     * Get the root view for this row
     */
    fun getView() : View {
        return itemView
    }

    /**
     * Get the current set value for this row
     */
    fun getEditText() : EditText {
        return itemView.edittext_value
    }

    /**
     * Focus on the EditText in the row
     */
    fun focus() {
        if(itemView.edittext_value.isFocusable) {
            itemView.edittext_value.requestFocus()
        }
    }


}