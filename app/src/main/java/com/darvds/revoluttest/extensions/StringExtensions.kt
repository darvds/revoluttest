package com.darvds.revoluttest.extensions

import java.util.*

/**
 * Convert the two letter country code to the emoji flag
 */
fun String.countryEmoji(): String {
    if (this.length != 2) return ""
    val firstLetter = Character.codePointAt(this.toUpperCase(Locale.ENGLISH), 0) - 0x41 + 0x1F1E6
    val secondLetter = Character.codePointAt(this.toUpperCase(Locale.ENGLISH), 1) - 0x41 + 0x1F1E6
    return String(Character.toChars(firstLetter)) + String(Character.toChars(secondLetter))
}
