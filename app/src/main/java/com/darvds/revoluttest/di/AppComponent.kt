package com.darvds.revoluttest.di

import com.darvds.revoluttest.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkingModule::class,
        AppModule::class
    ]
)
interface AppComponent {

    fun inject(activity: MainActivity)

}