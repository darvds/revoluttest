## Revolut Test App
This app is a simple app to show Exchange Rates for various currencies. 
Currencies will update automatically every 1 second while the app is in the foreground.
Selecting a different currency will move it to the top and make it the main currency to convert to.

## App Structure
The app is built using MVP, with Dagger for dependency injection. RxJava is used for network calls. 
Junit test are used for all presenters and business logic, with Mockito used to mock Android code away from the tests.
