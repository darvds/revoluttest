package com.darvds.revoluttest.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.darvds.revoluttest.R
import com.darvds.revoluttest.model.CurrencyRow
import com.darvds.revoluttest.utils.CustomTextWatcher
import javax.inject.Inject

/**
 * Adapter for showing a list of currencies in a recycler view
 */
class MainAdapter @Inject constructor() : RecyclerView.Adapter<CurrencyViewHolder>() {

    @Inject
    lateinit var layoutInflater: LayoutInflater

    lateinit var callback: MainAdapterCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = layoutInflater.inflate(R.layout.itemrow, parent, false)
        return CurrencyViewHolder(view, CustomTextWatcher(callback), callback)
    }

    override fun getItemCount(): Int {
        return callback.getItemCount()
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currencyRow = callback.getItem(position)
        holder.bind(currencyRow, position)
    }


    interface MainAdapterCallback {
        fun getItemCount(): Int
        fun getItem(position: Int): CurrencyRow
        fun onItemPressed(position: Int)
        fun setBaseValue(newValue: String)
    }
}