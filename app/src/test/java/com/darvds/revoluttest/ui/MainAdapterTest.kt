package com.darvds.revoluttest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.darvds.revoluttest.R
import com.darvds.revoluttest.model.CurrencyRow
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.android.synthetic.main.itemrow.view.*
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainAdapterTest {

    private val adapter = MainAdapter()

    private val mockLayoutInflater = mock<LayoutInflater>()

    private val mockCallback = mock<MainAdapter.MainAdapterCallback>()

    private val mockViewHolder = mock<CurrencyViewHolder>()

    @Before
    fun setUp() {

        adapter.layoutInflater = mockLayoutInflater
        adapter.callback = mockCallback
    }

    @Test
    fun onCreateViewHolder_retuns_viewholder(){
        val mockParent = mock<ViewGroup>()
        val mockView = mock<View>()
        whenever(mockView.edittext_value).thenReturn(mock())

        whenever(mockLayoutInflater.inflate(R.layout.itemrow, mockParent, false))
            .thenReturn(mockView)

        val viewHolder = adapter.onCreateViewHolder(mockParent, 0)

        assertEquals(mockView, viewHolder.itemView)
    }

    @Test
    fun getItemCount_returns_value_from_callback(){
        whenever(mockCallback.getItemCount()).thenReturn(42)
        assertEquals(42, adapter.itemCount)
    }

    @Test
    fun onBindViewHolder_binds_view(){
        val mockCurrencyRow = mock<CurrencyRow>()
        whenever(mockCallback.getItem(20)).thenReturn(mockCurrencyRow)

        adapter.onBindViewHolder(mockViewHolder, 20)

        verify(mockViewHolder).bind(mockCurrencyRow, 20)
    }
}