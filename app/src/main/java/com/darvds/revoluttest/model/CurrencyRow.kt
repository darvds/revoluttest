package com.darvds.revoluttest.model

import java.text.NumberFormat

class CurrencyRow(
    val countryCode: String,
    val currency: String,
    val currencyName: String
) {

    /**
     * The current currency value after converting
     */
    var value: Double = 0.0

    /**
     * Update the value of the exchange based on the exchange rate data.
     * I've used a switch here for simplicity, but
     * I could have also used reflection to avoid a long switch
     */
    fun updateExchangeRate(currentCurrency: String, baseValue: Double, exchangeRate: ExchangeRate) {
        if(currentCurrency == currency){
            return
        }

        val rate = when (currency) {
            "AUD" -> exchangeRate.rates.AUD
            "BGN" -> exchangeRate.rates.BGN
            "BRL" -> exchangeRate.rates.BRL
            "CAD" -> exchangeRate.rates.CAD
            "CHF" -> exchangeRate.rates.CHF
            "CNY" -> exchangeRate.rates.CNY
            "CZK" -> exchangeRate.rates.CZK
            "DKK" -> exchangeRate.rates.DKK
            "EUR" -> exchangeRate.rates.EUR
            "GBP" -> exchangeRate.rates.GBP
            "HKD" -> exchangeRate.rates.HKD
            "HRK" -> exchangeRate.rates.HRK
            "HUF" -> exchangeRate.rates.HUF
            "IDR" -> exchangeRate.rates.IDR
            "ILS" -> exchangeRate.rates.ILS
            "INR" -> exchangeRate.rates.INR
            "ISK" -> exchangeRate.rates.ISK
            "JPY" -> exchangeRate.rates.JPY
            "KRW" -> exchangeRate.rates.KRW
            "MXN" -> exchangeRate.rates.MXN
            "MYR" -> exchangeRate.rates.MYR
            "NOK" -> exchangeRate.rates.NOK
            "NZD" -> exchangeRate.rates.NZD
            "PHP" -> exchangeRate.rates.PHP
            "PLN" -> exchangeRate.rates.PLN
            "RON" -> exchangeRate.rates.RON
            "RUB" -> exchangeRate.rates.RUB
            "SEK" -> exchangeRate.rates.SEK
            "SGD" -> exchangeRate.rates.SGD
            "THB" -> exchangeRate.rates.THB
            "TRY" -> exchangeRate.rates.TRY
            "USD" -> exchangeRate.rates.USD
            "ZAR" -> exchangeRate.rates.ZAR
            else -> 1.0
        }

        value = baseValue * rate
    }


    /**
     * Return the converted currency in the correct format
     */
    fun getFormattedValue(numberFormat: NumberFormat) : String {
        return numberFormat.format(value)
    }


}