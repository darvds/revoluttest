package com.darvds.revoluttest.utils


import io.reactivex.Observable
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit

interface ObservableHelper {

    fun getRepeatingObservable(initialDelay: Long, timePeriod: Long, timeUnit: TimeUnit) : Observable<Long>

    fun getMainThreadScheduler() : Scheduler
}