package com.darvds.revoluttest.ui

import com.darvds.revoluttest.api.RevolutApi
import com.darvds.revoluttest.model.CurrencyRow
import com.darvds.revoluttest.model.ExchangeRate
import com.darvds.revoluttest.utils.ObservableHelper
import com.darvds.revoluttest.utils.ObservableHelperImpl
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.NumberFormat
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Presenter for the main activity of the app.
 *
 * Loads exchange rate data from the API and then displays it in a list.
 */
class MainPresenter @Inject constructor() : MainAdapter.MainAdapterCallback {

    /**
     * The API for polling the exchange rate
     */
    @Inject
    lateinit var api: RevolutApi

    /**
     * Adapter to display the data on the UI
     */
    @Inject
    lateinit var adapter: MainAdapter

    /**
     * The list of currencies to display
     */
    private lateinit var currencies: ArrayList<CurrencyRow>

    /**
     * Helper class for RxJava, that can be unit tested
     */
    var observableHelper: ObservableHelper = ObservableHelperImpl()

    /**
     * Reference to the UI
     */
    private var view: View? = null

    /**
     * Disposable for the API to stop it when the view is no longer visible
     */
    private var exchangeRateSubscriber: Disposable? = null

    /**
     * The list of exchange rates, based on the current selected currency
     */
    private var exchangeRate: ExchangeRate? = null


    interface View {
        fun setAdapter(adapter: MainAdapter)
        fun updateRows(startPosition: Int, count: Int)
        fun scrollToPosition(position: Int)
        fun focusItem(position: Int)
        fun showError(error: Throwable?)

    }

    /**
     * Start the presenter
     */
    fun start() {
        adapter.callback = this
        createCurrencies()
    }

    /**
     * Attach the view to the presenter
     */
    fun attachView(view: View) {
        this.view = view
    }

    /**
     * When the view is no longer on screen, stop downloading new api data to save on battery life
     */
    fun detachView() {
        exchangeRateSubscriber?.dispose()
        view = null
    }

    /**
     * Start loading the exchange rate at an interval of one second, for a specific currency
     */
    fun startLoadingExchangeRate() {

        exchangeRateSubscriber = Observable.interval(0, 1, TimeUnit.SECONDS)
            .flatMap { api.getExchangeRate(getCurrentCurrency().currency) }
            .observeOn(observableHelper.getMainThreadScheduler())
            .retry()
            .subscribe(
                { exchangeRate ->
                    this.exchangeRate = exchangeRate
                    updateExchangeRate(getCurrentCurrency().value)
                },
                { error ->
                    onError(error)
                })
    }

    /**
     * When new data is retrieved for the exchange rate, update all rows in the view,
     * except the first row
     */
    private fun updateExchangeRate(rate: Double) {
        exchangeRate?.let { xr ->
            currencies.drop(1).forEach { currencyRow -> currencyRow.updateExchangeRate(getCurrentCurrency().currency, rate, xr) }
            updateRows()
        }
    }

    /**
     * Handle an error from the api. Show the error to the user
     */
    private fun onError(error: Throwable?) {
        view?.showError(error)
    }

    /**
     * Create the list of currencies to display in the view. Hard code GBP to be
     * first in the list for testing purposes only and set a default value of £100
     */
    private fun createCurrencies() {
        currencies = ArrayList()
        currencies.add(CurrencyRow("GB", "GBP", "British Pound").apply {
            value = 100.0
        })
        currencies.add(CurrencyRow("AU", "AUD", "Australian Dollar"))
        currencies.add(CurrencyRow("BG", "BGN", "Bulgarian Lev"))
        currencies.add(CurrencyRow("BR", "BRL", "Brazilian Real"))
        currencies.add(CurrencyRow("CA", "CAD", "Canadian Dollar"))
        currencies.add(CurrencyRow("CH", "CHF", "Swiss Franc"))
        currencies.add(CurrencyRow("CN", "CNY", "Chinese Yuan"))
        currencies.add(CurrencyRow("CZ", "CZK", "Czech Koruna"))
        currencies.add(CurrencyRow("DK", "DKK", "Danish Krone"))
        currencies.add(CurrencyRow("EU", "EUR", "Euro"))
        currencies.add(CurrencyRow("HK", "HKD", "Hong Kong Dollar"))
        currencies.add(CurrencyRow("HR", "HRK", "Croatian Kuna"))
        currencies.add(CurrencyRow("HU", "HUF", "Hungarian Forint"))
        currencies.add(CurrencyRow("ID", "IDR", "Indonesian Rupiah"))
        currencies.add(CurrencyRow("IL", "ILS", "Israeli New Shekel"))
        currencies.add(CurrencyRow("IN", "INR", "Indian Rupee"))
        currencies.add(CurrencyRow("IS", "ISK", "Icelandic Krona"))
        currencies.add(CurrencyRow("JP", "JPY", "Japanese Yen"))
        currencies.add(CurrencyRow("KR", "KRW", "South Korean Won"))
        currencies.add(CurrencyRow("MX", "MXN", "Mexican Peso"))
        currencies.add(CurrencyRow("MY", "MYR", "Malaysian Ringgit"))
        currencies.add(CurrencyRow("NO", "NOK", "Norwegian Krone"))
        currencies.add(CurrencyRow("NZ", "NZD", "New Zealand Dollar"))
        currencies.add(CurrencyRow("PH", "PHP", "Philippine Peso"))
        currencies.add(CurrencyRow("PL", "PLN", "Polish Zloty"))
        currencies.add(CurrencyRow("RO", "RON", "Romanian Leu"))
        currencies.add(CurrencyRow("RU", "RUB", "Russian Ruble"))
        currencies.add(CurrencyRow("SE", "SEK", "Swedish Krona"))
        currencies.add(CurrencyRow("SG", "SGD", "Singapore Dollar"))
        currencies.add(CurrencyRow("TH", "THB", "Thai Baht"))
        currencies.add(CurrencyRow("TR", "TRY", "Turkish Lira"))
        currencies.add(CurrencyRow("US", "USD", "US Dollar"))
        currencies.add(CurrencyRow("ZA", "ZAR", "South African Rand"))

        view?.setAdapter(adapter)
        adapter.notifyDataSetChanged()

    }

    /**
     * Move the specified currency to the top of the list and update the adapter
     * to change the view
     */
    private fun moveCurrencyToTop(item: CurrencyRow) {
        val existingPosition = currencies.indexOf(item)
        currencies.remove(item)
        currencies.add(0, item)

        //Move the items in the adapter
        adapter.notifyItemMoved(existingPosition, 0)

        //Update rows that have changed
        adapter.notifyItemRangeChanged(0, existingPosition + 1)

        //Scroll to the top and focus
        view?.scrollToPosition(0)
        view?.focusItem(0)
    }

    /**
     * Get the number of currencies to display
     */
    override fun getItemCount(): Int {
        return currencies.size
    }

    /**
     * Get the currency at this position
     */
    override fun getItem(position: Int): CurrencyRow {
        return currencies[position]
    }

    /**
     * When a currency row is pressed, if it isn't at the top already,
     * move it to the top
     */
    override fun onItemPressed(position: Int) {
        if (position > 0) {
            moveCurrencyToTop(getItem(position))
        }
    }

    /**
     * Set the base value to convert the other currencies into. This will affect the
     * current 1st currency in the list
     */
    override fun setBaseValue(newValue: String) {
        if (newValue.isNotEmpty()) {
            val numberFormat = NumberFormat.getNumberInstance()
            val value = numberFormat.parse(newValue)?.toDouble() ?: 0.0
            val currencyRow = getCurrentCurrency()
            if(value != currencyRow.value) {
                currencies[0].value = value
                updateExchangeRate(value)
            }
        } else {
            updateExchangeRate(0.0)
        }
    }

    /**
     * Update all of the rows except the 1st one
     */
    private fun updateRows() {
        view?.updateRows(1, currencies.size - 1)
    }

    /**
     * Get the current currency
     */
    private fun getCurrentCurrency(): CurrencyRow {
        return currencies[0]
    }


}