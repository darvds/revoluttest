package com.darvds.revoluttest.di

import com.darvds.revoluttest.api.RevolutApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkingModule {

    companion object{
        const val baseUrl = "https://revolut.duckdns.org/"
    }

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit) : RevolutApi {
        return retrofit.create(RevolutApi::class.java)
    }

    @Provides
    @Singleton
    fun getRetrofit(client: OkHttpClient.Builder, gson: Gson) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client.build())
            .build()
    }

    @Provides
    fun provideOkHttpBuilder(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
    }

    @Provides
    fun provideGson() : Gson {
        return GsonBuilder().create()
    }

}