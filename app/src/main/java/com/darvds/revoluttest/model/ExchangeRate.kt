package com.darvds.revoluttest.model

data class ExchangeRate(
    val base: String,
    val date: String,
    val rates: Rates
)
