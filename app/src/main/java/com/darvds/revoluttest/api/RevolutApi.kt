package com.darvds.revoluttest.api

import com.darvds.revoluttest.model.ExchangeRate
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("latest")
    fun getExchangeRate(@Query("base") baseCurrency: String) : Observable<ExchangeRate>
}