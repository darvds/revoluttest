package com.darvds.revoluttest.ui

import com.darvds.revoluttest.api.RevolutApi
import com.darvds.revoluttest.model.ExchangeRate
import com.darvds.revoluttest.utils.ObservableHelper
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    private lateinit var presenter: MainPresenter

    private val mockView = mock<MainPresenter.View>()

    private val mockApi = mock<RevolutApi>()

    private val mockAdapter = mock<MainAdapter>()

    private val mockObservableHelper = mock<ObservableHelper>()

    @Before
    fun setUp() {
        presenter = MainPresenter()
        presenter.api = mockApi
        presenter.adapter = mockAdapter
        presenter.observableHelper = mockObservableHelper

        presenter.attachView(mockView)
    }

    @Test
    fun start_sets_callback(){
        presenter.start()

        verify(mockAdapter).callback = presenter
    }

    @Test
    fun start_creates_countries(){
        presenter.start()

        assertEquals(33, presenter.getItemCount())

        verify(mockView).setAdapter(mockAdapter)
        verify(mockAdapter).notifyDataSetChanged()
    }

    @Test
    fun getItem_0_returns_first_row(){
        presenter.start()

        val currencyRow = presenter.getItem(0)
        assertEquals("GBP", currencyRow.currency)
    }

    @Test
    fun startLoadingExchangeRate_success_sets_exchange_rate(){

        val testScheduler = Schedulers.trampoline()
        whenever(mockObservableHelper.getMainThreadScheduler()).thenReturn(testScheduler)

        val mockExchangeRate = mock<ExchangeRate>()

        doReturn(Observable.just(mockExchangeRate)).whenever(mockApi).getExchangeRate("GBP")

        presenter.start()
        presenter.startLoadingExchangeRate()

        testScheduler.start()
    }

    @Test
    fun onItemPressed_first_does_nothing(){
        presenter.start()
        presenter.onItemPressed(0)

        verify(mockAdapter, never()).notifyItemMoved(any(), any())
    }

    @Test
    fun onItemPressed_second_item_moves_to_top(){
        presenter.start()
        presenter.onItemPressed(1)

        assertEquals("AUD", presenter.getItem(0).currency)
        assertEquals("GBP", presenter.getItem(1).currency)

        verify(mockAdapter).notifyItemMoved(1,0)
        verify(mockAdapter).notifyItemRangeChanged(0,2)

        verify(mockView).scrollToPosition(0)
        verify(mockView).focusItem(0)
    }

    @Test
    fun setBaseValue_empty_resets_all_to_zero(){
        presenter.start()

        presenter.setBaseValue("")

        assertEquals(0.0, presenter.getItem(1).value, 0.0)
    }

    @Test
    fun setBaseValue_same_value_does_nothing(){
        presenter.start()

        presenter.setBaseValue("100")

        verify(mockView, never()).updateRows(any(), any())
    }


}
