package com.darvds.revoluttest.ui

import com.darvds.revoluttest.utils.CustomTextWatcher
import org.junit.Before
import org.junit.Test
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify

import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CustomTextWatcherTest {

    private lateinit var textWatcher: CustomTextWatcher

    private val mockCallback = mock<MainAdapter.MainAdapterCallback>()

    @Before
    fun setUp() {
        textWatcher = CustomTextWatcher(mockCallback)
    }

    @Test
    fun onTextChanged_position_0_sends_new_value() {
        textWatcher.updatePosition(0)
        textWatcher.onTextChanged("20.0", 0, 0, 0)

        verify(mockCallback).setBaseValue("20.0")
    }

    @Test
    fun onTextChanged_position_0_null_value_sends_0() {
        textWatcher.updatePosition(0)
        textWatcher.onTextChanged(null, 0, 0, 0)

        verify(mockCallback).setBaseValue("0.0")
    }

    @Test
    fun onTextChanged_position_1_sends_new_value() {
        textWatcher.updatePosition(1)
        textWatcher.onTextChanged("20.0", 0, 0, 0)

        verify(mockCallback, never()).setBaseValue(anyString())
    }
}