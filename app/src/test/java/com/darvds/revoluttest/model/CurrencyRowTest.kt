package com.darvds.revoluttest.model

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.text.NumberFormat

@RunWith(MockitoJUnitRunner::class)
class CurrencyRowTest {

    @Test
    fun updateExchangeRate_same_currency_does_nothing(){

        val currencyRow = CurrencyRow("GB", "GBP", "British Pound").apply {
            value = 100.0
        }

        currencyRow.updateExchangeRate("GBP", 200.0, mock())

        assertEquals(100.0, currencyRow.value, 0.0)
    }

    @Test
    fun updateExchangeRate_changes_rate(){

        val currencyRow = CurrencyRow("US", "USD", "US Dollar").apply {
            value = 0.0
        }

        val mockExchangeRate = mock<ExchangeRate>()
        val mockRates = mock<Rates>()
        whenever(mockRates.USD).thenReturn(2.0)
        whenever(mockExchangeRate.rates).thenReturn(mockRates)

        currencyRow.updateExchangeRate("GBP", 100.0, mockExchangeRate)

        assertEquals(200.0, currencyRow.value, 0.0)
    }

    @Test
    fun getFormattedValue_returns_formatted_number(){

        val currencyRow = CurrencyRow("GB", "GBP", "British Pound").apply {
            value = 100.0
        }

        val mockNumberFormat = mock<NumberFormat>()
        whenever(mockNumberFormat.format(100.0)).thenReturn("100.0")

        assertEquals("100.0", currencyRow.getFormattedValue(mockNumberFormat))
    }
}